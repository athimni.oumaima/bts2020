-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 03 Avril 2020 à 20:55
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bts2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `demandef`
--

CREATE TABLE IF NOT EXISTS `demandef` (
  `idDemande` int(10) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `cin` int(10) NOT NULL,
  `add` varchar(100) NOT NULL,
  `tel` int(10) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `niveau` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `addprojet` varchar(100) NOT NULL,
  `composante` varchar(100) NOT NULL,
  `cout` varchar(255) NOT NULL,
  `montantp` varchar(255) NOT NULL,
  `autofinancement` varchar(255) NOT NULL,
  `montant` varchar(255) NOT NULL,
  `file` varchar(200) NOT NULL,
  `etat` int(1) NOT NULL,
  PRIMARY KEY (`idDemande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `inscricompt`
--

CREATE TABLE IF NOT EXISTS `inscricompt` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `etat` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `inscricompt`
--

INSERT INTO `inscricompt` (`id`, `nom`, `prenom`, `mail`, `mdp`, `etat`) VALUES
(1, 'azertyy', 'azeert', 'azerty@gmail.com', 'ab4f63f9ac65152575886860dde480a1', 0),
(2, 'oumaima', 'oumaima', 'tiamomimouta@gmail.com', 'ab4f63f9ac65152575886860dde480a1', 0),
(3, 'test', 'test', 'test@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 0),
(4, 'test2', 'test2', 'test2@gmail.com', 'ad0234829205b9033196ba818f7a872b', 0),
(6, 'athimni', 'athimni', 'athimni94@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(7, 'bts', 'bts', 'bts@gmail.com', 'c3ea886e7d47f5c49a7d092fadf0c03b', 0),
(25, 'client', 'client', 'client@gmail.com', '62608e08adc29a8d6dbc9754e659f125', 0),
(26, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0),
(28, 'oumaima', 'ssss', 'oumaima@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(29, 'ameni', 'werhani', 'werhani@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(31, 'hassen', 'athimni', 'hassen@gmail.com', 'cc7c8424138a7558ada97e11a916feba', 0),
(32, 'eee', 'eee', 'eee@gmail.com', 'd2f2297d6e829cd3493aa7de4416a18f', 0);

-- --------------------------------------------------------

--
-- Structure de la table `reclamation`
--

CREATE TABLE IF NOT EXISTS `reclamation` (
  `idrec` int(10) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `recla` varchar(255) NOT NULL,
  `daterec` text NOT NULL,
  `etat` int(1) NOT NULL,
  PRIMARY KEY (`idrec`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(0, 'admin', ''),
(1, 'admin', 'admin'),
(2, 'user', 'user'),
(3, 'Admin2', 'admin'),
(4, 'admin4', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
