<?php
session_start();

function verifySession()
{
  if(!isset($_SESSION['cin']) or !isset($_SESSION['nom']) )
  {  deconnexion();}
}

$message=false;

if (isset($_POST['nom']) & isset($_POST['prenom'])& isset($_POST['cin']) & isset($_POST['add'])& isset($_POST['tel'])& isset($_POST['mail'])& isset($_POST['niveau'])& isset($_POST['type'])& isset($_POST['titre'])& isset($_POST['addprojet'])& isset($_POST['composante'])& 
  isset($_POST['cout'])& isset($_POST['montantp'])& isset($_POST['autofinancement']))
{
$message=demander($_SESSION['nom'],$_SESSION['prenom'],$_SESSION['cin'],$_POST['add'],$_POST['tel'],$_SESSION['mail'],$_SESSION['niveau'],
  $_SESSION['type'],$_SESSION['titre'],$_SESSION['addprojet'],$_SESSION['composante'],$_SESSION['cout'],$_SESSION['montantp'],
  $_SESSION['autofinancement']);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<style type="text/css">

input[type="submit"],
input[type="reset"] {
  background-color: #666;
  border: 1px solid #fff;
  border-radius: .25em;
  padding: 5px 10px;
}
.form-search, .form-contact {
  background-color: #ededed;
  margin: 0 auto;
  padding: .5em;
  width: 60%
}
.form-search {
  margin-bottom: 1em;
  text-align: right;
}

.search-term, .search-submit {
  margin: 0;
  padding: .25em;
}

fieldset {
  border: 10px;
}

legend {
  font-weight: bold;
  left: 0;
  padding: .5em 0;
  position: relative;
  width: 100%;
}

label, 
input[type="text"],
input[type="email"],
input[type="tel"],
input[type="file"],
textarea,

select {
  display: inline-block;
  margin-bottom: .25em;
  padding: 3px 5px;
  width: 45%;
}

label {
  text-align: right;
}

.contact-submit {
  margin-left: 46%;
}
</style>
<script langauge='javascript'>
function verifier()
{}
</script>
<body>
<?php include("ProfilClient.php"); ?>

<form class="form-contact" action="insertDemandeFinancement.php" method="POST" name="verifierDemande">
   <br><br> 
  <fieldset id="personal-contact">
    <legend>Information Client</legend>

      <label for="nom"> Nom </label>
      <input type="text" id="nome" placeholder=""  name="nom"  />

      <label for="prenom"> Prénom </label>
      <input type="text" id="prenom" placeholder="" name="prenom" />

      <label for="cin">Numéro d'identification national</label>
      <input type="text" id="cin" placeholder="" name="cin" />

      <label for="add">Adresse</label>
      <input type="text"  placeholder="" name="add"  />

      <label for="tel">Numéro de téléphone</label>
      <input type="tel"  placeholder=""  name="tel" />

      <label for="mail">E-mail</label>
      <input type="email" id="mail" placeholder="" name="mail" />

      <label for="niveau">Niveau d'étude</label>
      <input type="text"  placeholder="" name="niveau" />

  </fieldset>

  

  <fieldset id="physical-contact">
    <legend>Le Projet</legend>

     <label for="type" name="type" >Type du projet</label>
    <select id="type">
      <option id="default" selected>Choisier le type...&hellip;</option>
      <option id="type-nouveau"> Nouveau </option>
      <option id="type-expansion"> Expansion </option>
      <option id="type-agent"> Agent en circulation </option>
    </select>

    <label for="titre"> Titre du projet </label>
    <input type="text" id="" placeholder="" name="titre"/>

    <label for="addprojet"> Adresse du projet </label>
    <input type="text" id="" placeholder="" name="addprojet"  />
        <br><br>
     <label for="composante"> Composante du projet </label>
     <textarea id="composante" name="composante" > </textarea>


    <label for="cout"> Cout estime du projet </label>
    <input type="text" id="" placeholder="" name="cout"  />

    <label for="montant"> Le montant du projet </label>
    <input type="text" id="" placeholder="" name="montantp"  />

    <label for="autofinancement"> Autofinancement </label>
    <input type="text" id="" placeholder=""  name="autofinancement" />

    <label for="montantp"> Le montant des versements de récuparation proposés </label>
    <input type="text" id="" placeholder="" name="montantp" />
      <br><br>

    
    
    
  </fieldset>
  <input type="submit" value="Envoyer" name="envoyer" class="btn contact-submit" onclick="verifier()"  />
  <input type="reset" value="Annuler" class="btn contact-clear" />
</form>

</body>
</html>