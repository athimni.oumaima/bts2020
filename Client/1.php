<!DOCTYPE html>
<html lang="fr">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex, nofollow">
    <title>Profil</title>
    <link rel="stylesheet" href="https://s3-eu-west-1.amazonaws.com/io.mailtrack.assets/backend/66c89ed/dist/styles.6f811da00210802fa4e4.css">

    <style>
        .max_700 {max-width: 700px;}
        .border-0 {
            border: none !important;
            box-shadow: none;
            padding: 0;
                }
        .w100 {width: 100%;}
    </style>
</head>

<body class="dashboard">
<div class="container-fluid">
<div class="row">
<div class="col-sm-3 col-md-2 sidebar">
            
    <div class="menu-head">
        <a href="#toggle" class="menu-toggle"><i class="icon-menu"></i></a>
        <span class="label label-free">Bienvenu</span>
    </div>

   
    <ul class="nav nav-sidebar">
        <li class="active">
        <a href="#"><i class="icon-user"></i> Mon Profiel</a>
       </li>
        <li>
        <a href="#" target="_blank"><i class="icon-question"></i>Des Informations</a>
        </li>

        <li>
        <a href="Reclamation.php"><i class="icon-people"></i>Demande De Financement</a>
        </li>

        <li>
        <a href="#"><i class="icon-logout"></i>Déconnxion</a>
        </li>

    </ul>
</div>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h1 class="page-header"> <i class="icon-user"></i>Profile</h1>
<div class="row">
<div class="col-md-8">
<div class="content-box">
 <div class="content-box-inner">
    <h4 class="sub-header">Mon Profilé</h4>
    <form action="#" method="post" class="" novalidate="novalidate">
        <div class="row max_700">
            <div class="col-md-6">
                <div id="" class="form-group  ">
                    <label class="">Nom</label>
                    <input type="text" id="account_user_name" name="account_user[name]" required="required" placeholder=""  class="form-control " value="" />
                </div>
            </div>

            <div class="col-md-6">
                <div id="" class="form-group">
                    <label class="">Prénom</label>
                    <input type="text" id="account_user_surname" name="account_user[surname]" required="required" placeholder=""  class="form-control " value="" />
                </div>
            </div>
        </div>
            <p><button type="submit" class="btn btn-primary">Modifié Compt</button></p>
    </form>
 </div>
</div>
</div>
</div>
</div>
        </div>
    </div>






                                 
           
       
      

    

    

   
    <script src="https://s3-eu-west-1.amazonaws.com/io.mailtrack.assets/backend/66c89ed/dist/dashboard.36bfd0c11ebab54102be.js"></script>

    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script>

    <script type="text/javascript" src="https://s3-eu-west-1.amazonaws.com/io.mailtrack.assets/backend/66c89ed/dist/dashboardAccount.a8f7ec86f48d8e3dad54.js"></script>
</body>
</html>
