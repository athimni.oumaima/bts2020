<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>les Coordonnees De Demande</title>
</head>
<style type="text/css">
   

input[type="submit"],
input[type="reset"] {
  background-color: #666;
  border: 1px solid #fff;
  border-radius: .25em;
  padding: 5px 10px;
}
.form-search, .form-contact {
  background-color: #ededed;
  margin: 0 auto;
  padding: .5em;
  width: 60%
}
.form-search {
  margin-bottom: 1em;
  text-align: right;
}

.search-term, .search-submit {
  margin: 0;
  padding: .25em;
}

fieldset {
  border: 0;
}

legend {
  font-weight: bold;
  left: 0;
  padding: .5em 0;
  position: relative;
  width: 100%;
}

label, 
input[type="text"],
input[type="email"],
input[type="tel"],
input[type="file"],
textarea,

select {
  display: inline-block;
  margin-bottom: .25em;
  padding: 3px 5px;
  width: 45%;
}

label {
  text-align: right;
}

.contact-submit {
  margin-left: 46%;
}
</style>

<body>
<?php include("ProfilClient.php"); ?>

<form class="form-contact" action="" method="get" >
   <br><br> 
  <fieldset id="personal-contact">
    <legend>Information Client</legend>

      <label for="nom"> Nom </label>
      <input type="text" value=""  name="nom"  />

      <label for="prenom"> Prénom </label>
      <input type="text"  value="" name="prenom" />

      <label for="cin">Numéro d'identification national</label>
      <input type="text" id="cin" value="" name="cin" />

      <label for="add">Adresse</label>
      <input type="text"  value="" name="add"  />

      <label for="tele">Numéro de téléphone</label>
      <input type="tel"  value=""  name="tel" />

      <label for="mail">E-mail</label>
      <input type="email" id="mail" value="" name="mail" />

      <label for="niveau">Niveau d'étude</label>
      <input type="text"  value="" name="niveau" />

  </fieldset>

  

  <fieldset id="physical-contact">
    <legend>Le Projet</legend>

     <label for="type" name="type" >Type du projet</label>
    <select id="type">
      <option id="default" selected>Choisier le type...&hellip;</option>
      <option id="type-nouveau"> Nouveau </option>
      <option id="type-expansion"> Expansion </option>
      <option id="type-agent"> Agent en circulation </option>
    </select>

    <label for="titre"> Titre du projet </label>
    <input type="text"  value="" name="titre"/>

    <label for="addprojet"> Adresse du projet </label>
    <input type="text"  value=""name="addprojet"  />
        <br><br>
     <label for="composante"> Composante du projet </label>
     <textarea  value="" name="composante" > </textarea>


    <label for="cout"> Cout estime du projet </label>
    <input type="text"  value="" name="cout"  />

    <label for="montant"> Le montant du projet </label>
    <input type="text"  value="" name="montantp" />

    <label for="autofinancement"> Autofinancement </label>
    <input type="text" value=""  name="autofinancement" />

    <label for="montant-proposes"> Le montant des versements de récuparation proposés </label>
    <input type="text"  value="" name="montantp" />
      <br><br>

    
    <center><input type="submit"  value="Modifiér" name="modifier" /></center>    
    

    
  </fieldset>

</form>

</body>
</html>