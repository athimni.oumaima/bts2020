<?php
session_start();

require_once("connexion.php");

$rech=isset($_GET['recherche'])?$_GET['recherche']:"";
$nom=isset($_GET['nom'])?$_GET['nom']:"";

$cin=isset($_GET['cin'])?$_GET['cin']:"all";
if($cin=="all")
  { $req = " SELECT * FROM personne WHERE nom LIKE '%$nom%' "; }
else 
    { $req="SELECT * FROM personne WHERE  nom LIKE '%$nom%' AND cin='$cin' "; }
$res=$pdo->query($req);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Liset les Personnes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

  <?php include("menu.php"); ?>
<body>
  
<div class="container">
    <br>
  <div class="panel panel-default margetop60">
    <div class="panel-body"> 
     <form method="get" action="personne.php" class="form-inline">
      <div class="form-group"> 
       <input type="text" name="recherche" placeholder="Taper le nom de la personne" class="form-control  ">
          <button type="submit" class="btn btn-success" name="recherche_nom"> 
            <span  class="glyphicon glyphicon-search"></span>  chercher ... 
          </button>
                   &nbsp &nbsp          
          <a href ="ajouterPersonne.php">
            <span  class="glyphicon glyphicon-plus"></span>  Nouvelle Personne
          </a>
    </form>
    </div>
        <br><br>

      <div class="panel panel-primary margetop10">
        <div class="panel-heading">Liset de la Personne</div>
          <div class="panel-body">
            <form method="get" action="ajouter.php" class="form-inline">
              <table class="table table-striped table-border">
                  <thead>
                     <tr>
                       <th> Id_Personne  </th>
                       <th> Nom </th>
                       <th> Prénom </th>
                       <th>Type</th>
                       <th>Actions</th>
                     </tr>
                  </thead>

                  <tbody>
                    <?php
                              while($personne=$res->fetch()){
                                ?>
                      <tr>
                              <td><?php echo $personne['Idpersonne']?></td>
                              <td><?php echo $personne['nom']?></td>
                              <td><?php echo $personne['prenom']?></td> 
                              <td><?php echo $personne['type']?></td> 
                          
                         <td>
                            <a href="modifier.php ?id=<?php echo $personne['Idpersonne']?>"> <span class="glyphicon glyphicon-edit"> </span> <a/>
                              &nbsp;
                            <a onclick="return confirm('Etes vous sur de vouloir supprime un personne')" href="supprimePersonne.php ?id=<?php echo $personne['Idpersonne']?>"> 
                            <span class="glyphicon glyphicon-trash"> </span> <a/>
                         </td>
                       </tr>
                         
                    <?php } ?>

                  </tbody>
              </table>
            </form> 
             </div>

          </div>

</div>

</body>
</html>
