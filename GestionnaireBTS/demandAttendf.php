<?php
require_once("connexion.php");

$nom=isset($_GET['nom'])?$_GET['nom']:"";
$prenom=isset($_GET['prenom'])?$_GET['prenom']:"";
$cin=isset($_GET['cin'])?$_GET['cin']:"";
$req="SELECT * FROM demandef WHERE etat=0 '";
$res=$pdo->query($req);
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Demandes en Attente</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<?php include("lisetFinancement.php"); ?>
<div class="container">
<div class="panel panel-success margetop">
<div class="panel-body">
<form method="get" action="demandAttendf.php" class="form-inline">
</form>
<div class="container">
<div class="panel panel-primary">
<div class="panel-heading">Demandes En Attende</div> 
<div class="panel-body">
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Id Demande</th>
<th>Nom</th>
<th>Prenom</th>
<th>CIN</th>
<th>Action</th>
</tr>
</thead>

<tbody>
<?php
while($de=$res->fetch()){?>
<tr class="<?php echo $de['etat']==0?'info':'info' ?>">
	<td><?php echo $de['idDemande']?></td>
	<td><?php echo $de['nom']?></td>
	<td><?php echo $de['prenom']?></td>
	<td><?php echo $de['cin']?></td> 
   <td>
		<a href="information.php"><span class="glyphicon glyphicon-edit"> </span></a>
			&nbsp;&nbsp;
	 <a onclick="return confirm('Etes !! vous sur de vouloir supprimer le demande')"
	 href="supprimeDemande.php"><span class="glyphicon glyphicon-trash"> </span></a>
	 &nbsp;&nbsp;
	 
	 <a href="reponseDemandephp.php ?idDemande=<?php echo $de['idDemande']?>&etat=<?php echo $de['etat']?>">
	   <span class="glyphicon glyphicon-exclamation-sign"></span>
		<?php
			if($de['etat']==2)
			 echo'<span class="glyphicon glyphicon-remove"></span>';
			 elseif($de['etat']==1)
				 echo'<span class="glyphicon glyphicon-ok"></span>';
			 else
			 echo'<span class="glyphicon glyphicon-exclamation-sign"></span>';
	 	?>
	 </a>
	 <a href="information.php ?idDemande=<?php echo $de['idDemande']?>"><span class="fas fa-info-circle"></span></a>
   </td>
</tr>
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</body>
</html>