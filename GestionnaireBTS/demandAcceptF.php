<?php
require_once("connexion.php");

$nom=isset($_GET['nom'])?$_GET['nom']:"";
$prenom=isset($_GET['prenom'])?$_GET['prenom']:"";
$cin=isset($_GET['cin'])?$_GET['cin']:"";
$req="SELECT* FROM demandef WHERE etat=1";
$res=$pdo->query($req);
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Demandes Accepte</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<?php include("lisetFinancement.php"); ?>
<div class="container">
<div class="panel panel-success margetop">
<div class="panel-body">
<form method="get" action="demanderefause.php" class="form-inline">
</form>
<div class="container">
<div class="panel panel-primary margetop">
<div class="panel-heading">Demandes Accepte</div> 
<div class="panel-body">
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Id Demande</th>
<th>CIN</th>
<th>Nom</th>
<th>Prenom</th>
<th>Action</th>

</tr>
</thead>
<tbody>

<?php
while($de=$res->fetch()){?>
<tr class="<?php echo $de['etat']==1?'success':'danger' ?>">
<td><?php echo $de['idDemande']?></td>
<td><?php echo $de['cin']?></td>
<td><?php echo $de['nom']?></td>
<td><?php echo $de['prenom']?></td>

<td>
<a href="" ><span class="glyphicon glyphicon-edit"> </span></a>
&nbsp;&nbsp;
<a onclick="return confirm('Etes !! vous sur de vouloir supprimer le demande')"
 href=""><span class="glyphicon glyphicon-trash"> </span></a>
 &nbsp;&nbsp;
<a href="information.php ?idDemande=<?php echo $de['idDemande']?>"><span class="glyphicon glyphicon-book"> </span></a>
&nbsp;&nbsp;
</td>
</tr>
<?php } ?>	

</tbody>
</table>
</div>
</div>
</div>
</body>
</html>
