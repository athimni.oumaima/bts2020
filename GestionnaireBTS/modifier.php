<?php
require_once("connexion.php");

$id=isset($_GET['Idpersonne'])?$_GET['Idpersonne']:0;


$req="SELECT * FROM personne WHERE Idpersonne=$id";
$res=$pdo->query($req);
$mod=$res->fetch();
$nom=$mod['nom'];
$prenom=$mod['prenom'];
$cin=$mod['cin'];
$type=$mod['type'];
$login=$mod['login'];
$pwd=$mod['pwd'];


?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Ajouter un personne </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style> margetop {margin-top=60px;} </style>
<body>
 <?php include("menu.php"); ?>
<div class="container">

  <div class="panel panel-primary margetop ">

    	<div class="panel-heading">Modification les données de la  Personne</div>
      <div class="panel-body"> 
	   	  	<form method="post" action="modifierPersonne.php" class="form">

		  		<div class="form-group">
					<label for="nom">Nom : </label>
		 			<input type="text" placeholder="" class="form-control" name="nom" value="<?php echo $nom ?>">
             	</div>

	 			<div class="form-group">
					<label for="prenom">Prénom: </label>
					<input type="text" placeholder="" class="form-control" name="prenom" value="<?php echo $prenom ?>">
			   	</div>	

	 			<div class="form-group">
					<label for="cin">CIN: </label>
		            <input type="text" placeholder="" class="form-control" name="cin" value="<?php echo $cin ?>">
	            </div>		

				 <div class="form-group">
					<label for="type">Type: </label>
		            <input type="text" placeholder="" class="form-control" name="type" value="<?php echo $type ?>">
	            </div>		  

	 			<div class="form-group">
					<label for="login">Login: </label>
		            <input type="text" placeholder="" class="form-control" name="login" value="<?php echo $login ?>">
	            </div>	

	 			<div class="form-group">
					<label for="pwd">Password: </label>
		            <input type="password" placeholder="" class="form-control" name="pwd" value="<?php echo $pwd ?>">
				</div>&nbsp;&nbsp; 

				<button type="submit" class="btn btn-success" name="enregitrement"> 
				 	<span  class="glyphicon glyphicon-save"></span> Enregitrement Modification
			    </button>
			 </form>    
		</div>
	</div>	
</div> 

	
     
</body>
</html>