<?php
require_once("connexion.php");


$nom=isset($_GET['nom'])?$_GET['nom']:"";
$prenom=isset($_GET['prenom'])?$_GET['prenom']:"";
$cin=isset($_GET['cin'])?$_GET['cin']:"";

$req="SELECT * FROM demandef";

$res=$pdo->query($req);
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Toutes les demandes</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<?php include("lisetFinancement.php"); ?>
<div class="container">
<div class="panel panel-success margetop">
<div class="panel-heading">Rechercher..</div> 
<div class="panel-body">
<form method="get" action="toutdemande.php" class="form-inline">
<div class="form-group">
<input type="text" name="t1" placeholder="Taper le nom " class="form-control">
<button type="sumbit"  name="b" class="btn btn-success"><span class="glyphicon glyphicon-search"> </span>Rechercher</button>
</div>
</div>
</div>

<div class="container">
<div class="panel panel-primary margetop">
<div class="panel-heading">Toutes les demandes</div> 
<div class="panel-body">
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Id demande</th>
<th>Nom</th>
<th>Prenom</th>
<th>CIN</th>
<th>Action</th>

</tr>
</thead>
<tbody>

<?php
while($de=$res->fetch()){?>
<tr class="<?php  if($de['etat']==1)echo 'success'; elseif($de['etat']==2 ) echo 'danger'; else echo'info'?>">
<td><?php echo $de['idDemande']?></td>
<td><?php echo $de['nom']?></td>
<td><?php echo $de['prenom']?></td>
<td><?php echo $de['cin']?></td> 
<td>
<a href="information.php ?idDemande=<?php echo $de['idDemande']?>"><span class="glyphicon glyphicon-book"> </span></a>
&nbsp;&nbsp;

<a onclick="return confirm('Etes !! vous sur de vouloir supprimer le demande')"
 href="suppd.php  ?idDemande=<?php echo $de['idDemande']?>"><span class="glyphicon glyphicon-trash"> </span></a>
 &nbsp;&nbsp;
  <a href="reponse.php ?idDemande=<?php echo $de['idDemande']?>&etat=<?php echo $de['etat']?>">
  
  <?php
 if($de['etat']==1)
 echo'<span class="glyphicon glyphicon-remove"></span>';
elseif($de['etat']==2)
	 echo'<span class="glyphicon glyphicon-ok"></span>';
else
	echo '<span class="glyphicon glyphicon-exclamation-sign"></span>';
 ?>
 </a>
</td>
</tr>	
<?php } ?>

</tbody>
</table>
</div>
</div>
</div>
</body>
</html>
